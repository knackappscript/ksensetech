/**
* Ksense tech 
*/

'strict'

+function(){
	
	this.KsensetechView = function(app, conf){

		/**
		 * Scope config for application view framework
		 */
		let config = (conf||{});

		/**
		 * Table 
		 */
		app.table = function(view_id){

			let model = Knack.views[view_id].model.data.models;

			return {
				fields : function(args){
					model.map(function(e, i){
						if(typeof model[i].fields !== 'object') model[i].fields = {};
						for(var field in args){
							model[i].fields[field] = eval("model[i].attributes."+args[field]);
						};
					});
				},
				records : function(i = 0){
					return model[i].fields;
				},
				model	: model

			}
		};

		/**
		 * Details Object
		 */
		app.detail = function(view_id){
			
			let model = Knack.views[view_id].record;

			return {
				fields : function(args){
					for(var field in args){
						model.fields[field] = eval("model."+args[field]);
					}
				},
				records : model.fields,
				model	: model
			}
		}
	}

}();