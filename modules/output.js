/**
* Ksense tech 
*/

'strict'

+function(){
  
  this.KsensetechOutput = function(app, conf){

    /**
     * Scope config for application output csv framework
     */
    let config = (conf||{});
    
    _default = { // default setting
      separator : ','
    };

    var opt = Object.assign(_default, config);

    /**
     * Output csv : data register 
     * NotifyException : function with internal method info, warning, error, success of the notify types
     */
    app.csv = function(header, data, NotifyException){

      var h = header||[], d = data||[];

      _NotifyException = function(msg, type){
        switch(type){
          case 'warning' : 
            alert(msg); break;
          case 'error' : 
            alert(msg); break;
          case 'success' : 
            alert(msg); break;
          case 'info' : 
            alert(msg); break;
          default :
            alert(msg); break;
        }
      };

      var UserNotify = NotifyException||_NotifyException;

      return {
        data : function(header, data){
          h = header||[], d = data||[];
        },
        save : function(fileName, mimeType){

          // data = h; data.concat(d);
          data = [];
          data.push(h); 

          d.forEach(function(e, i){ data.push(d); });


          fileName = fileName.indexOf('.csv')!=-1?fileName:`${fileName}.csv`;
          
          var content = "", csvContent = '';
          
          if(typeof data !== 'object' || data.length <= 0){
            UserNotify('warning', 'Sorry the csv file is empty'); return;
          }

          data.forEach(function(infoArray, index) {
            dataString = infoArray.join(opt.separator);
            csvContent += index < data.length ? dataString + "\n" : dataString;
          });

          content = csvContent;

          var a = document.createElement('a');
          
          mimeType = mimeType || 'application/octet-stream';

          if (navigator.msSaveBlob) {
            navigator.msSaveBlob(new Blob([content], {
              type: mimeType
            }), fileName);
          } else if (URL && 'download' in a) {
            a.href = URL.createObjectURL(new Blob([content], {
              type: mimeType
            }));
            a.setAttribute('download', fileName);
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
          } else {
            location.href = 'data:application/octet-stream,' + encodeURIComponent(content);
          }

        }

      }
    };

    // output to excel file xlsx
    app.xlsx = function(NotifyException, add_dependency = false){

      function _setDependency(url){
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = url;
        document.head.appendChild(s);
      }

      var _urlXlsxFull = 'https://glcdn.githack.com/knackappscript/ksensetech/raw/master/modules/output_libraries/excel_output/xlsx.full.min.js';
      var _urlFileSaver = 'https://glcdn.githack.com/knackappscript/ksensetech/raw/master/modules/output_libraries/excel_output/FileSaver.min.js';

      if(add_dependency === true && typeof XLSX === "undefined"){
        _setDependency(_urlXlsxFull);
        _setDependency(_urlFileSaver);
      }else if(add_dependency === true && typeof XLSX.write !== 'function'){
        _setDependency(_urlFileSaver);
      }

      _NotifyException = function(msg, type){
        switch(type){
          case 'warning' : 
            alert(msg); break;
          case 'error' : 
            alert(msg); break;
          case 'success' : 
            alert(msg); break;
          case 'info' : 
            alert(msg); break;
          default :
            alert(msg); break;
        }
      };

      var UserNotify = NotifyException||_NotifyException;

      /**
       * Initializate wb WorkBook Sheet
       */
      var wb = {SheetNames:[], Sheets:{}}, WorkBookName = 'Report.xlsx';
      
      // buffer save 
      function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
      }

      return {
        _setRowEmptyHeader : function(index){
          let d = {}; d[index] = [];
          return {
            add : function(arr){
              let r = {}; 
              arr.map(function(e,i){ r[" ".repeat(i+1)] = e });
              d[index].push(r);
            },
            JSON : function(){
              return d[index];
            }
          }
        },
        workbook : function(){
          return wb;
        },
        newSheet : function(sheet_name, json_data, conf){
          // create new sheet and return the instance
          default_conf = {"!cols" : [],"!ref"  : "","!merges":[]};
          let _config;

          if(conf){
            _config = Object.assign(default_conf, conf||{});
          }

          if(json_data){
            let data = XLSX.utils.json_to_sheet(json_data);
            wb.SheetNames.push(sheet_name); wb.Sheets[sheet_name] = data;
            if(_config){
              wb.Sheets[sheet_name] = Object.assign(_config, wb.Sheets[sheet_name]);
            }
          }else{
            wb.SheetNames.push(sheet_name);
          }

          return wb.Sheets[sheet_name];
        },
        conf : function(sheet_name, conf){
          // add config to cell
          default_conf = {"!cols" : [],"!ref"  : "","!merges":[]};
          let _config = Object.assign(default_conf, conf||{});
          wb.Sheets[sheet_name] = Object.assign(_config, wb.Sheets[sheet_name]);
          return wb;
        },
        dataJSON : function(sheet_name, json_data, conf){
          default_conf = {"!cols" : [],"!ref"  : "","!merges":[]};
          let data = XLSX.utils.json_to_sheet(json_data);
          wb.Sheets[sheet_name] = data;
          let _config = Object.assign(default_conf, conf||{});
          wb.Sheets[sheet_name] = Object.assign(_config, wb.Sheets[sheet_name]);
        },
        save : function(file_name){
          WorkBookName = -1!=file_name.indexOf('.xlsx')?file_name:`${file_name}.xlsx`;
          var blob = new Blob([s2ab(XLSX.write(wb, {bookType:'xlsx', bookSST:true, tabSelected : 1, showGridLines : false, cellStyles: true, type:'binary'}))], {
              type: "application/octet-stream"
          });
          saveAs(blob, WorkBookName);
        }


      };

    }

    
  }

}();